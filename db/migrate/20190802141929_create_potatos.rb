class CreatePotatos < ActiveRecord::Migration[5.2]
  def change
    create_table :potatos do |t|
      t.string :content
      t.boolean :happy
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
