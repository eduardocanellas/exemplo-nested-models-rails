Rails.application.routes.draw do
  root 'people#index'
  resources :people, path: 'pessoas', path_names: { new: 'criar', edit: 'editar' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
