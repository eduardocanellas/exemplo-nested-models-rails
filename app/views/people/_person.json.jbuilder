json.extract! person, :id, :name, :age, :student, :birth, :created_at, :updated_at
json.url person_url(person, format: :json)
