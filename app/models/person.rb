# frozen_string_literal: true

class Person < ApplicationRecord
  has_many :potatoes, dependent: :destroy

  accepts_nested_attributes_for(
    :potatoes,
    reject_if: lambda { |attrbs| attrbs['content'].blank? },
    allow_destroy: true
  )
  # Lambda: basicamente uma função anônima em ruby.
end
