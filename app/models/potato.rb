class Potato < ApplicationRecord
  belongs_to :person
  validates_presence_of :content
  validates :content, length: { in: 20..50 }
end
