module PotatoesHelper
  def potato_happy_text(potato)
    if potato.happy
      'Batata feliz!'
    else
      'Batata triste :('
    end
  end
end
